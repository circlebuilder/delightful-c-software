# delightful c software [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of of lightweight, cross-platform C programs and libraries.  All programs are Free, Libre and/or Open Source with source code available.  They're lightweight and cross-platform compatible with minimal dependencies, so they should be easy to build from source on most operating systems with a C compiler.

## Contents

- [Contact](#contact)
- [List](#list)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Contact

You're welcome to make suggestions for other libraries and programs using the issues or by posting suggestions via [Mastodon](https://scholar.social/web/@lmemsm/107984595685026944).

## List

| Program | Summary|
| :---: | --- |
|[bard](https://github.com/festvox/bard)|Ebook reader with text-to-speech support using SDL and flite.  I have some patches for this program to improve portability and support using SDL2.|
|[BearSSL](https://bearssl.org/)|Rather secure implementation of the SSL/TLS protocol.  Can be used with curl.|
|[cal](http://unicorn.us.com/cal.html)|Command line calendar.|
|[cDetect](http://cdetect.sourceforge.net/)|C based alternative to GNU configure/autoconf.  More info at IngwiePhoenix's [cDetect project](https://github.com/IngwiePhoenix/cDetect).  Plus, I've forked the project and use it with many of my builds from source code.  I've added support for cross-compiling and many, many other features.  Contact me if you'd like a copy.  I currently have an [older version](http://www.distasis.com/cpp/lmports.htm) of my modifications uploaded.|
|[csvutils](https://github.com/rgamble/csvutils)|CSV command utilities.  Uses libcsv.|
|[curl](https://curl.se/)|Command line data transfer tool.|
|[diff](https://lists.suckless.org/dev/1601/28247.html)|diff implementation for sbase.  Check the follow-up mailing list threads for further patches.|
|[diffh](https://sourceforge.net/projects/diffh/)|Works with diff and creates an easy to read display of differences between files in HTML format.|
|[dr_libs](https://github.com/mackron/dr_libs)|Single file header audio decoding libraries.|
|[fcurl](https://github.com/curl/fcurl)|Library to simplify working with curl.|
|[BSD gettext](https://www.mmnt.net/db/0/18/ftp.khstu.ru/pub/unix/distfiles)|Older BSD gettext/libintl implementation.  I have a fork of this one as well.  Also, check out the BSD Citrus Project.|
|[BSD gzip](https://github.com/NetBSD/src/tree/trunk/usr.bin/gzip)|BSD version of the gzip compression/decompression program.  There are various forks to port this to operating systems other than BSD.  I have a portable fork as well.|
|[gifsicle](http://www.lcdf.org/gifsicle/)|GIF animator utility.|
|[grafx2](http://grafx2.chez.com/)|Graphics editor using SDL.|
|[less](https://www.greenwoodsoftware.com/less/)|Less is more than more, pager program.|
|[libarchive](https://www.libarchive.org/)|Archive and compression library, bsdtar and bsdcpio.|
|[libcsv](https://github.com/rgamble/libcsv)|ANSI C library to read and write CSV files.|
|[liblzw](https://github.com/vapier/liblzw)|Library for LZW (.Z) compression/decompression.|
|[libtomcrypt](https://github.com/libtom/libtomcrypt)|Public Domain cryptography library.|
|[lxsplit](http://lxsplit.sourceforge.net/)|Command line file split/join tool.|
|[man](https://github.com/jbruchon/elks)|C program to view standard man pages.|
|[minzip](http://zlib.net/)|A zip library for zlib. Useful when working with files in zip format. Code is in the contrib section of zlib.|
|[nanosvg](https://github.com/memononen/nanosvg)|Lightweight SVG library.|
|[nanotodon](https://github.com/taka-tuos/nanotodon)|Lightweight TUI/C99 Mastodon client which uses ncurses (or pdcurses) and curl.|
|[ncurses hexedit](http://www.rogoyski.com/adam/programs/hexedit/)|Curses based hex editor.|
|[nemini](https://github.com/neonmoe/nemini)|Lightweight SDL2 based Gemini client.|
|[BSD patch](https://github.com/openbsd/src/tree/master/usr.bin/patch)|BSD fork of the patch program.  I have a fork of this as well with some portability additions to better handle carriage return/line feed issues.|
|[pdfconcat](https://github.com/pts/pdfconcat)|Concatenates PDF files.|
|[pdftxt](https://litcave.rudi.ir/)|Convert PDF to text. Helpful for searching PDFs with grep.|
|[pkgconf](https://github.com/pkgconf/pkgconf)|Drop in replacement for pkg-config with no circular dependencies.|
|[picaxo](http://gigi.nullneuron.net/comp/picaxo/)|Graphics viewer using SDL or SDL2.  I have patches to add SDL2 support.  Contact me if you'd like a copy.|
|[pspg](https://github.com/okbob/pspg)|Postgres pager provides a console based pager for PostgreSQL, MySQL, CSV and other formats.  Uses ncurses (or pdcurses).
|[shot](https://github.com/rr-/shot/)|Command line screenshot program.|
|[sox](http://sox.sourceforge.net/)|Sound exchange utility and library. Converts sound formats. Plays audio files.|
|[stb](https://github.com/nothings/stb)|Public Domain single file header libraries.  Includes stb_truetype.h, a lightweight alternative to the freetype library, which can parse, decode and rasterize characters from truetype fonts.
|[unarr](https://github.com/sumatrapdfreader/sumatrapdf/tree/master/ext/unarr)|Decompression library for rar and other formats. Part of SumatraPDF project.|
|[x509cert](https://github.com/michaelforney/x509cert)|Generate x509 certificate requests. Works with BearSSL.|

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/lmemsm/delightful-c-software/issues) with the tracker.

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/master/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC by 4.0](https://licensebuttons.net/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)
